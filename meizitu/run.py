#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @File  : run.py
# @Author: slowchen
# @Date  : 2018/6/7 20:26
# @Desc  : 爬虫启动文件，运行爬虫请执行当前文件

import os
import time

from scrapy import cmdline

now = time.strftime("%Y%m%d%H%M%S", time.localtime())
os.makedirs(os.path.join(os.getcwd(), 'log'), exist_ok=True)

# 控制台显示log
# cmdline.execute("scrapy crawl mzt".split())

# log保存在log文件夹里面，加时间戳
cmdline.execute(("scrapy crawl mzt -s LOG_FILE=log/mzt_%s.log" % now).split())
